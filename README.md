# Guide de déploiement


## Initialisation
- Cloner le répo
- Ouvrez un terminal dans le dossier du répo 
- Creer un environment virtuel venv 
- Installer avec pip les différentes dependances (voir requirements.txt)


## Activation en local
- Dans le terminal de votre dossier executez les commandes suivante pour lancer le projet en local
- ```source venv/bin/activate```
- ```export FLASK_APP=run```
- ```flask run```


## Preparation au déploiement

- Remplacer ```http://127.0.0.1:5000``` par l'url de votre site en ligne (par exemple ```https://micro-microblog.herokuapp.com/```) à ces differents endroits :
 - Dans le fichier views.py au lignes 74, 106 et 110
 - Supprimer le contenu du dossier DATA (ne supprimez pas le dossier)


## Deploiement

- Deployer enfin le projet sur l'hebergeur de votre choix en suivant leur procédure de mise en ligne 



# Autres infos


## Tests postman

- en local : https://www.getpostman.com/collections/d72c927041c33216661c
- en ligne : mise en ligne toujours pas successful



## Lien de l'application (marche pas encore)

- https://micro-microblog.herokuapp.com/


